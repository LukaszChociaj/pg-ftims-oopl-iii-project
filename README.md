Object-Oriented Programming Languages III final project
Podstawowe informacje
Autor: Łukasz Chociaj
Data rozpoczęcia projektu: 15.01.2016
Mail: chotek1994@gmail.com
Wszystkie prawa zastrzeżone 
Informacje o projekcie:

Projekt to prosty symulator biletomatu, umożliwiający zakup różnych rodzajów biletów, odczyt historii zakupów, oraz dochodu biletomatu.

Aby skompilować program należy wejść do głównego folderu projektu w konsoli systemowej i użyć tam polecenia java

## javac src/*.java ##


Aby uruchomić program należy w folderze "src" projektu w konsoli użyć polecenia 

## java main ##