/**
 * Created by Lukasz on 15.01.2016.
 */
public class ReducedTicket extends  Ticket {

    private double reduction;


    public ReducedTicket(int r, double prize){

        super(prize*(100-r)/100);
        this.reduction=100-r;
        setName("Bilet Ulgowy");


    }

    public double getReduction(){

        return reduction;

    }

    public void setReduction(double reduction){

        this.reduction=reduction;

    }


}
