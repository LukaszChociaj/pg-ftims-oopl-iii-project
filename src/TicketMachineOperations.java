import java.util.Scanner;

/**
 * Created by Lukasz on 16.01.2016.
 */
public class TicketMachineOperations extends TicketMachine {



    public void makeSomething() throws WrongButtonException{

        int input;
        Scanner s = new Scanner(System.in);
        boolean exit = false;

        do{
            System.out.println("Press '1' to buy normal ticket ");
            System.out.println("Press '2' to buy reduced ticket ");
            System.out.println("Press '3' to buy night ticket ");
            System.out.println("Press '4' to buy reduced night ticket ");
            System.out.println("Press '5' to view ticket machine history ");
            System.out.println("Press '6' to view ticket machine income ");
            System.out.println("Press '0' to exit");

            input = s.nextInt();

            switch (input){

                case 1: buyTicket(NORMAL_TICKET);
                    System.out.println("Zakupiono bilet normalny.");
                    break;

                case 2: buyTicket(REDUCED_TICKET);
                    System.out.println("Zakupiono bilet ulgowy.");
                    break;

                case 3: buyTicket(NIGHT_TICKET);
                    System.out.println("Zakupiono bilet nocny.");
                    break;

                case 4: buyTicket(NIGHT_REDUCED_TICKET);
                    System.out.println("Zakupiono bilet nocny ulgowy.");
                    break;

                case 5: showTickets();
                    break;

                case 6: System.out.println("Income of the ticket machine is: " + getIncome() + " PLN");
                    break;

                case 0:  exit = true;
                    break;

                default: throw new WrongButtonException();

            }

        }while(!exit);

    }

}
