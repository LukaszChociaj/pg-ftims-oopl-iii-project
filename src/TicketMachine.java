import java.util.*;
import java.text.SimpleDateFormat;

/**
 * Created by Lukasz on 16.01.2016.
 */
public class TicketMachine implements About {

    public static final int NORMAL_TICKET=0;
    public static final int REDUCED_TICKET=1;
    public static final int NIGHT_TICKET=2;
    public static final int NIGHT_REDUCED_TICKET=3;
    private Map<Integer,Ticket> tickets;
    private int income=0;
    private Map<Ticket,List<String>> ticketMap;
    private Map<String,Ticket> sortedTickets;
    public TicketMachine(){

        tickets = new TreeMap<>();
        tickets.put(NORMAL_TICKET,new Ticket(2.0));
        tickets.put(REDUCED_TICKET,new ReducedTicket(50,2.0));
        tickets.put(NIGHT_TICKET,new SpecialTicket(3.0));
        tickets.put(NIGHT_REDUCED_TICKET,new SpecialReducedTicket(25,3.0));

        ticketMap= new TreeMap<>(
                (p1,  p2) -> p2.getName().compareTo(p1.getName())
        );


        ticketMap.put(tickets.get(NORMAL_TICKET),new ArrayList<>());
        ticketMap.put(tickets.get(REDUCED_TICKET),new ArrayList<>());
        ticketMap.put(tickets.get(NIGHT_TICKET),new ArrayList<>());
        ticketMap.put(tickets.get(NIGHT_REDUCED_TICKET),new ArrayList<>());

        sortedTickets= new TreeMap<>();
    }

    @Override
    public void writeAboutMe(){

        System.out.println("Autorem programu jest Lukasz Chociaj. Program to prosty biletomat umozliwiajacy zakup kilku " +
                "rodzajow biletow, oraz ");
        System.out.println("wyswietlenie historii transakcji, oraz dochodu biletomatu ");

    }
    private void addIncome(int type){

        income+= tickets.get(type).getPrize();

    }
    public void buyTicket(int kindOfTicket){

        String timeStamp = new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss:SS").format(Calendar.getInstance().getTime());
        ticketMap.get(tickets.get(kindOfTicket)).add(timeStamp);
        sortedTickets.put(timeStamp,tickets.get(kindOfTicket));
        addIncome(kindOfTicket);

    }

    public void showTickets(){

        for (String name: sortedTickets.keySet()){

            String value = sortedTickets.get(name).getName();
            System.out.println(name + " " + value);
        }

    }
    public int getIncome(){

        return income;
    }

}
