/**
 * Created by Lukasz on 15.01.2016.
 */
public class Ticket implements MyTickets{

    private double prize = 2;
    private String name = "Bilet normalny";


    public Ticket(double _prize) {

        prize = _prize;

    }

    @Override
    public double getPrize() {

        return prize;

    }

    @Override
    public void setPrize(double _prize) {

        prize = _prize;

    }

    @Override
    public String getName(){

        return name;

    }

    @Override
    public void setName(String name){

        this.name=name;

    }
}