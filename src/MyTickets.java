/**
 * Created by Lukasz on 17.01.2016.
 */
public interface MyTickets {

    double getPrize();
    void setPrize(double prize);
    String getName();
    void setName(String name);
}
