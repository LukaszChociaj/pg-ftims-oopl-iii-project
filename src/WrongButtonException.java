/**
 * Created by Lukasz on 16.01.2016.
 */
public class WrongButtonException extends Exception {

    @Override
    public String getMessage(){

        return "Wrong button! Try Again!";

    }
}
